package pl.codementors.mages.model;

import lombok.*;

/**
 * Created by sit0 on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Cores {

    private int id;

    private String name;

    private int power;

    private int consistency;

    public Cores (String name, int power, int consistency) {
        this.name = name;
        this.power = power;
        this.consistency = consistency;
    }

}
