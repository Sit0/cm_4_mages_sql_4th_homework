package pl.codementors.mages.menu;

import pl.codementors.mages.database.CoresDAO;
import pl.codementors.mages.model.Cores;

import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class CoresManager extends BaseManager<Cores, CoresDAO> {

    public CoresManager() {
        dao = new CoresDAO();
    }

    @Override
    protected Cores parseNew(Scanner scanner) {
        System.out.print("Enter new name: ");
        String name = scanner.next();
        System.out.print("Enter new power: ");
        int power = scanner.nextInt();
        System.out.print("Enter new consistency: ");
        int consistency = scanner.nextInt();
        return new Cores(name, power, consistency);
    }

    @Override
    protected void copyId(Cores from, Cores to) {
        to.setId(from.getId());
    }
}
