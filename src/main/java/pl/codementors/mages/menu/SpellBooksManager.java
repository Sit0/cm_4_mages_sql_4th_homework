package pl.codementors.mages.menu;

import pl.codementors.mages.database.SpellBooksDAO;
import pl.codementors.mages.database.SpellsDAO;
import pl.codementors.mages.model.SpellBooks;
import pl.codementors.mages.model.Spells;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class SpellBooksManager extends BaseManager<SpellBooks, SpellBooksDAO> {


    public SpellBooksManager() {
        dao = new SpellBooksDAO();
    }

    @Override
    protected SpellBooks parseNew(Scanner scanner) throws ParseException {
        System.out.print("Enter new title: ");
        String title = scanner.next();
        System.out.print("Enter new author: ");
        String author = scanner.next();
        System.out.print("Enter new publish date: ");
        String publishDateString = scanner.next();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy");
        java.util.Date date = formatter.parse(publishDateString);
        System.out.println("Enter new incantation");
        String incantation = scanner.next();
        Spells spells = new Spells(incantation);
        List<Spells> mageSpells = new SpellsDAO().createSpellList(spells);
        return new SpellBooks(title, author, date, mageSpells);
    }

    @Override
    protected void copyId(SpellBooks from, SpellBooks to) {
        to.setId(from.getId());
    }
}
