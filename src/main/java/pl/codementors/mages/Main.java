package pl.codementors.mages;

import pl.codementors.mages.menu.*;

import java.text.ParseException;
import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class Main {
    public static void main(String[] args) throws ParseException {

        Scanner scanner = new Scanner(System.in);
        String command;
        CoresManager coresManager = new CoresManager();
        MagesManager magesManager = new MagesManager();
        SpellBooksManager spellBooksManager = new SpellBooksManager();
        SpellsManager spellsManager = new SpellsManager();
        WandsManager wandsManager = new WandsManager();
        WoodsManager woodsManager = new WoodsManager();

        boolean run = true;

        while (run) {
            System.out.print("Enter Class <cores, mages, spellbooks, spells, wands, woods> or exit\n");
            command = scanner.next();

            switch (command) {
                case "cores": {
                    coresManager.manage(scanner);
                    break;
                }
                case "mages": {
                    magesManager.manage(scanner);
                    break;
                }
                case "spellbooks": {
                    spellBooksManager.manage(scanner);
                    break;
                }
                case "spells": {
                    spellsManager.manage(scanner);
                    break;
                }
                case "wands": {
                    wandsManager.manage(scanner);
                    break;
                }
                case "woods": {
                    woodsManager.manage(scanner);
                    break;
                }
                case "exit": {
                    run = false;
                    break;
                }
            }
        }
    }
}
