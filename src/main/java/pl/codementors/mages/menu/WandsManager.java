package pl.codementors.mages.menu;

import pl.codementors.mages.database.CoresDAO;
import pl.codementors.mages.database.WandsDAO;
import pl.codementors.mages.database.WoodsDAO;
import pl.codementors.mages.model.Cores;
import pl.codementors.mages.model.Wands;
import pl.codementors.mages.model.Woods;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class WandsManager extends BaseManager<Wands, WandsDAO> {

    public WandsManager() {
        dao = new WandsDAO();
    }

    @Override
    protected Wands parseNew(Scanner scanner) throws ParseException {
        System.out.print("Enter new wood ID: ");
        int wood = scanner.nextInt();
        System.out.print("Enter new core ID: ");
        int core = scanner.nextInt();
        System.out.println("Enter new product date");
        String dateString = scanner.next();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy MMM dd");
        Date date = formatter.parse(dateString);
        Woods woods = new WoodsDAO().find(wood);
        Cores cores = new CoresDAO().find(core);
        return new Wands(woods, cores, date);
    }

    @Override
    protected void copyId(Wands from, Wands to) {
        to.setId(from.getId());
    }
}
