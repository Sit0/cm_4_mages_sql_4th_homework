package pl.codementors.mages.model;

import lombok.*;

import java.util.Date;
import java.util.List;

/**
 * Created by sit0 on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class SpellBooks {

    private int id;

    private String title;

    private String author;

    private Date publishDate;

    private List<Spells> spells;

    public SpellBooks(String title, String author, Date publishDate, List<Spells> spells) {
        this.title = title;
        this.author = author;
        this.publishDate = publishDate;
        this.spells = spells;
    }
}
