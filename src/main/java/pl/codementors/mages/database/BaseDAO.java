package pl.codementors.mages.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sit0 on 09.07.17.
 */
public abstract class BaseDAO<T> {

    public static final Logger log = Logger.getLogger(BaseDAO.class.getName());

    public abstract String getTableName();

    public abstract T parseValue(ResultSet result) throws SQLException;

    public abstract Object[] getColumnsValues(T value);

    public abstract int getPrimaryKeyValue(T value);

    public abstract String[] getColumns();

    public void execute(String sql, Object[] params) {
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql.toString())) {
            setParams(statement, params);
            log.log(Level.INFO,"Executing query: " + statement.toString());
            statement.execute();
        } catch (SQLException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
    }

    public List<T> executeQuery(String sql, Object[] params) {
        List<T> values = new ArrayList<>();
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            setParams(statement, params);
            log.log(Level.FINE, statement.toString());
            try (ResultSet result = statement.executeQuery();) {
                while (result.next()) {
                    T value = parseValue(result);
                    values.add(value);
                }
            }
        } catch (SQLException ex) {
            log.log(Level.WARNING, ex.getMessage(), ex);
        }
        return values;
    }

    private void setParams(PreparedStatement statement, Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            Object param = values[i];
            int paramIndex = i + 1;
            statement.setObject(paramIndex,param);
        }
    }

    public void update(T value) {
        StringBuffer sql = new StringBuffer("UPDATE ");
        sql.append(getTableName()).append(" SET ");
        for (String column : getColumns()) {
            sql.append(column).append(" = ?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), " ");
        sql.append("WHERE id = ?");
        Object[] params = getColumnsValues(value);
        params = Arrays.copyOf(params, params.length + 1);
        params[params.length - 1] = getPrimaryKeyValue(value);
        execute(sql.toString(), params);
    }

    public void insert(T value) {
        StringBuffer sql = new StringBuffer("INSERT INTO ");
        sql.append(getTableName()).append(" (");
        for (String column : getColumns()) {
            sql.append(column).append(", ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        sql.append(" VALUES (");
        for (int i = 0; i < getColumns().length; i++) {
            sql.append("?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        execute(sql.toString(), getColumnsValues(value));
    }

    public void delete(int id) {
        String sql = "DELETE FROM " + getTableName() + "  WHERE id = ?";
        Object[] params = {id};
        execute(sql, params);
    }

    public T find(int id) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        Object[] params = {id};
        return executeQuery(sql, params).get(0);
    }
}
