package pl.codementors.mages.menu;

import pl.codementors.mages.database.WoodsDAO;
import pl.codementors.mages.model.Woods;

import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class WoodsManager extends BaseManager<Woods, WoodsDAO> {

    public WoodsManager() {
        dao = new WoodsDAO();
    }

    @Override
    protected Woods parseNew(Scanner scanner) {
        System.out.print("Enter new name: ");
        String name = scanner.next();
        System.out.print("Enter new toughness: ");
        int toughness = scanner.nextInt();
        return new Woods(name, toughness);
    }

    @Override
    protected void copyId(Woods from, Woods to) {
        to.setId(from.getId());
    }
}
