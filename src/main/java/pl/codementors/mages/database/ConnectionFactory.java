package pl.codementors.mages.database;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by sit0 on 09.07.17.
 */
public class ConnectionFactory {

    public static final Logger log = Logger.getLogger(ConnectionFactory.class.getName());

    private final static String URL = "jdbc:mysql://localhost:3306/mages";

    private final static String USER = "mage_admin";

    private final static String PASSWORD = "m4g34dm1n";

    private final static String PARAMS = "?useUnicode=true"
            + "&useJDBCCompliantTimezoneShift=true"
            + "&useLegacyDatetimeCode=false"
            + "&serverTimezone=UTC";

    private static BasicDataSource ds;

    public static Connection createConnection() throws SQLException {
        if (ds == null) {
            ds = new BasicDataSource();
            ds.setUsername(USER);
            ds.setPassword(PASSWORD);
            ds.setUrl(URL + PARAMS);
        }
        log.log(Level.CONFIG,"Database: " + URL);
        Connection connection = ds.getConnection();
        return connection;
    }
}
