package pl.codementors.mages.database;

import pl.codementors.mages.model.Woods;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sit0 on 09.07.17.
 */
public class WoodsDAO extends BaseDAO<Woods> {

    private String[] columns = {"name", "toughness"};

    @Override
    public String getTableName() {
        return "woods";
    }

    @Override
    public Woods parseValue(ResultSet result) throws SQLException {
        String name = result.getString(2);
        int toughness = result.getInt(3);
        return new Woods(name,toughness);
    }

    public Object[] getColumnsValues(Woods value) {
        Object[] values = {value.getName(), value.getToughness()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Woods value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }
}
