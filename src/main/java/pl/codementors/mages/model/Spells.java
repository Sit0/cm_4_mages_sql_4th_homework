package pl.codementors.mages.model;

import lombok.*;

/**
 * Created by sit0 on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Spells {

    private int id;
    private String incantation;

    public Spells(String incantation) {
        this.incantation = incantation;
    }
}
