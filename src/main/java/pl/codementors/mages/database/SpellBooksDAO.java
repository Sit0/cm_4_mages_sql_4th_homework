package pl.codementors.mages.database;

import pl.codementors.mages.model.SpellBooks;
import pl.codementors.mages.model.Spells;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

/**
 * Created by sit0 on 09.07.17.
 */
public class SpellBooksDAO extends BaseDAO<SpellBooks> {

    private String[] columns = {"title", "author", "publish_date"};

    @Override
    public String getTableName() {
        return "spells_book";
    }

    @Override
    public SpellBooks parseValue(ResultSet result) throws SQLException {
        int Id = result.getInt(1);
        String title = result.getString(2);
        String author = result.getString(3);
        Date publishYear = result.getDate(4);
        List<Spells> spells = new SpellsDAO().getSpellsBooks(Id);
        return new SpellBooks(title, author,publishYear,spells);
    }

    public Object[] getColumnsValues(SpellBooks value) {
        Object[] values = {value.getTitle(), value.getAuthor(), value.getPublishDate(), value.getSpells()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(SpellBooks value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }
}
