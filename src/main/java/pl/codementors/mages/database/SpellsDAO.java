package pl.codementors.mages.database;

import pl.codementors.mages.model.Spells;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by sit0 on 09.07.17.
 */
public class SpellsDAO extends BaseDAO<Spells> {

    private String[] columns = {"incantation"};

    @Override
    public String getTableName() {
        return "spells";
    }

    @Override
    public Spells parseValue(ResultSet result) throws SQLException {
        String incantation = result.getString(2);
        return new Spells(incantation);
    }

    public Object[] getColumnsValues(Spells value) {
        Object[] values = {value.getIncantation()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Spells value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    public List<Spells> getMagesSpells(int id) {
        String sql = "SELECT * FROM spell_books_spells WHERE mage = ID";
        Object[] params = {id};
        return executeQuery(sql, params);
    }

    public List<Spells> getSpellsBooks(int id) {
        String sql = "SELECT * FROM spell_books_spells WHERE book = ID";
        Object[] params = {id};
        return executeQuery(sql, params);
    }

    public List<Spells> createSpellList(Spells spells) {
        List<Spells> spellsToReturn = new ArrayList<>();
        spellsToReturn.add(spells);
        return spellsToReturn;
    }
}
