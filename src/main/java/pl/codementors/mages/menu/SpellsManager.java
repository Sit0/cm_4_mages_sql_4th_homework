package pl.codementors.mages.menu;

import pl.codementors.mages.database.SpellsDAO;
import pl.codementors.mages.model.Spells;

import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class SpellsManager extends BaseManager<Spells, SpellsDAO> {

    public SpellsManager() {
        dao = new SpellsDAO();
    }

    @Override
    protected Spells parseNew(Scanner scanner) {
        System.out.print("Enter new incantation: ");
        String incantation = scanner.next();
        return new Spells(incantation);
    }

    @Override
    protected void copyId(Spells from, Spells to) {
        to.setId(from.getId());
    }
}
