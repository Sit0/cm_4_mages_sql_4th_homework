package pl.codementors.mages.database;

import pl.codementors.mages.model.Mages;
import pl.codementors.mages.model.Spells;
import pl.codementors.mages.model.Wands;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by sit0 on 09.07.17.
 */
public class MagesDAO extends BaseDAO<Mages> {


    private String[] columns = {"name", "wand", "supervisor"};
    //TODO A Spell List

    @Override
    public String getTableName() {
        return "mages";
    }

    @Override
    public Mages parseValue (ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int wandId = result.getInt(3);
        int supervisorId = result.getInt(4);
        Wands wand = new WandsDAO().find(wandId);
        Mages supervisor = new MagesDAO().find(supervisorId);
        List<Spells> spells = new SpellsDAO().getMagesSpells(id);
        return new Mages(name,wand,supervisor,spells);
    }

    @Override
    public Object[] getColumnsValues(Mages value) {
        Object[] values = {value.getName(), value.getWand(), value.getSupervisor()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Mages value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }
}
