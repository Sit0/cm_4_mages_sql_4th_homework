package pl.codementors.mages.database;

import pl.codementors.mages.model.Cores;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sit0 on 09.07.17.
 */
public class CoresDAO extends BaseDAO<Cores> {

    private String[] columns = {"name", "power", "consistency"};

    @Override
    public String getTableName() {
        return "cores";
    }

    @Override
    public Cores parseValue (ResultSet result) throws SQLException {
        String name = result.getString(2);
        int power = result.getInt(3);
        int consistency = result.getInt(4);
        return new Cores(name,power,consistency);
    }

    @Override
    public Object[] getColumnsValues(Cores value) {
        Object[] values = {value.getName(), value.getPower(), value.getConsistency()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Cores value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }


}
