package pl.codementors.mages.menu;

import pl.codementors.mages.database.BaseDAO;

import java.text.ParseException;
import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public abstract class BaseManager<T, D extends BaseDAO<T>> {

    protected D dao;

    public void manage(Scanner scanner) throws ParseException {
        System.out.print("Enter command <add, find, delete, update>: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }

    protected abstract T parseNew(Scanner scanner) throws ParseException;

    protected abstract void copyId(T from, T to);

    private void update(Scanner scanner) throws ParseException {
        System.out.print("ID: ");
        int id = scanner.nextInt();
        T value = dao.find(id);
        System.out.println(value);
        T newValue = parseNew(scanner);
        copyId(value, newValue);
        dao.update(newValue);
    }

    private void delete(Scanner scanner) {
        System.out.print("ID: ");
        int id = scanner.nextInt();
        dao.delete(id);
    }

    private void find(Scanner scanner) {
        System.out.print("ID: ");
        int id = scanner.nextInt();
        T value = dao.find(id);
        System.out.println(value);
    }

    private void add(Scanner scanner) throws ParseException {
        T newValue = parseNew(scanner);
        dao.insert(newValue);
    }
}
