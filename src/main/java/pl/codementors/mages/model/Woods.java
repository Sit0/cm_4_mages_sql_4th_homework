package pl.codementors.mages.model;

import lombok.*;

/**
 * Created by sit0 on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Woods {

    private int id;

    private String name;

    private int toughness;

    public Woods(String name, int toughness) {
        this.name = name;
        this.toughness = toughness;
    }
}
