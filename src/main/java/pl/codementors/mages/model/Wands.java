package pl.codementors.mages.model;

import lombok.*;

import java.util.Date;

/**
 * Created by sit0 on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Wands {

    private int id;

    private Woods wood;

    private Cores core;

    private Date production_date;

    public Wands(Woods wood, Cores core, Date date) {
        this.wood = wood;
        this.core = core;
        this.production_date = date;
    }
}
