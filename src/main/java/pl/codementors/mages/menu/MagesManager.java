package pl.codementors.mages.menu;

import pl.codementors.mages.database.MagesDAO;
import pl.codementors.mages.database.SpellsDAO;
import pl.codementors.mages.database.WandsDAO;
import pl.codementors.mages.model.Mages;
import pl.codementors.mages.model.Spells;
import pl.codementors.mages.model.Wands;

import java.util.List;
import java.util.Scanner;

/**
 * Created by sit0 on 09.07.17.
 */
public class MagesManager extends BaseManager<Mages, MagesDAO> {

    public MagesManager() {
        dao = new MagesDAO();
    }

    @Override
    protected Mages parseNew(Scanner scanner) {
        System.out.print("Enter new name: ");
        String name = scanner.next();
        System.out.print("Enter new ID wand: ");
        int wand = scanner.nextInt();
        System.out.println("Enter new ID supervisor");
        int supervisorId = scanner.nextInt();
        System.out.println("Enter new ID book of spells");
        int spellId = scanner.nextInt();
        Wands wands = new WandsDAO().find(wand);
        Mages supervisor = new MagesDAO().find(supervisorId);
        List<Spells> spells = new SpellsDAO().getSpellsBooks(spellId);
        return new Mages(name, wands, supervisor, spells);
    }

    @Override
    protected void copyId(Mages from, Mages to) {
        to.setId(from.getId());
    }

}
