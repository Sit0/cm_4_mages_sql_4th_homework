package pl.codementors.mages.model;

import lombok.*;

import java.util.List;

/**
 * Created by sit0 on 09.07.17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString

public class Mages {

    private int id;
    private String name;
    private Wands wand;
    private Mages supervisor;
    private List<Spells> spells;


    public Mages(String name, Wands wand, Mages supervisor, List<Spells> spells) {
        this.name = name;
        this.wand = wand;
        this.supervisor = supervisor;
        this.spells = spells;
    }
}
