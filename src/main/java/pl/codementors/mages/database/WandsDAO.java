package pl.codementors.mages.database;

import pl.codementors.mages.model.Cores;
import pl.codementors.mages.model.Wands;
import pl.codementors.mages.model.Woods;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by sit0 on 09.07.17.
 */
public class WandsDAO extends BaseDAO<Wands> {

    private String[] columns = {"wood", "core", "production_date"};

    @Override
    public String getTableName() {
        return "wands";
    }

    @Override
    public Wands parseValue (ResultSet result) throws SQLException {

        int woodId = result.getInt(2);
        Woods woods = new WoodsDAO().find(woodId);
        int coreId = result.getInt(3);
        Cores cores = new CoresDAO().find(coreId);
        Date production_date = result.getDate(4);
        return new Wands(woods,cores,production_date);
    }

    @Override
    public Object[] getColumnsValues(Wands value) {
        Object[] values = {value.getWood(), value.getCore()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Wands value) {
        return value.getId();
    }

    @Override
    public String[] getColumns() {
        return columns;
    }
}
